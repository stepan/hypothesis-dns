from typing import cast, Tuple

import dns.message
import dns.name
import dns.query
import dns.rcode
import dns.rdataclass
import dns.rdatatype
import dns.rrset

from hypothesis import assume, given

import pytest

from strategies import dns_names, dns_rdatatypes_without_meta
from utils import *


# Just a place holder, till we hook it up in BIND repo properly
@pytest.fixture(scope="session")
def named_port() -> int:
    return 5300


# labels of a zone with * A 192.0.2.1 wildcard
SUFFIX = "allwild.test."
WILDCARD_RDTYPE = dns.rdatatype.A
WILDCARD_RDATA = "192.0.2.1"
IP_ADDR = "10.53.0.1"
TIMEOUT = 5  # seconds, just a sanity check


def tcp_query(
    where: str, port: int, qname: dns.name.Name, qtype: int
) -> Tuple[dns.message.Message, dns.message.Message]:
    qtype = cast(dns.rdatatype.RdataType, qtype)
    querymsg = dns.message.make_query(qname, qtype)
    assert_single_question(querymsg)
    return querymsg, dns.query.tcp(querymsg, where, port=port, timeout=TIMEOUT)


@given(name=dns_names(suffix=SUFFIX, min_labels=4), rdtype=dns_rdatatypes_without_meta)
def test_wildcard_rdtype_mismatch(name: dns.name.Name, rdtype: int, named_port: int) -> None:
    assume(rdtype != WILDCARD_RDTYPE)
    """any label non-matching rdtype must result in to NODATA"""
    query_msg, response_msg = tcp_query(IP_ADDR, named_port, name, rdtype)
    assert_is_response_to(response_msg, query_msg)
    assert_noerror(response_msg)
    assert_empty_answer(response_msg)


@given(name=dns_names(suffix=SUFFIX, min_labels=4))
def test_wildcard_match(name: dns.name.Name, named_port: int) -> None:
    """any label with maching rdtype must result in wildcard data in answer"""
    query_msg, response_msg = tcp_query(IP_ADDR, named_port, name, WILDCARD_RDTYPE)
    assert_is_response_to(response_msg, query_msg)
    assert_noerror(response_msg)
    assert_single_question(query_msg)
    expected_answer = [
        dns.rrset.from_text(
            query_msg.question[0].name,
            300,  # TTL, ignored by dnspython comparison
            dns.rdataclass.IN,
            WILDCARD_RDTYPE,
            WILDCARD_RDATA,
        )
    ]
    assert response_msg.answer == expected_answer, str(response_msg)
