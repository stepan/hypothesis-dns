from typing import Optional, List
from warnings import warn

from hypothesis.strategies import binary, builds, integers, just, lists, nothing, SearchStrategy

import dns.name
import dns.message
import dns.rdataclass
import dns.rdatatype

dns_labels = binary(min_size=1, max_size=63)


def dns_names(
    *, prefix: Optional[str] = None, suffix: Optional[str] = None, min_labels: int = 0, max_labels: int = 127
) -> SearchStrategy[dns.name.Name]:
    if prefix is not None:
        prefix_name = dns.name.from_text(prefix)
        prefix_name.choose_relativity(dns.name.root)
    else:
        prefix_name = dns.name.empty

    if suffix is not None:
        suffix_name = dns.name.from_text(suffix)
        suffix_name.derelativize(dns.name.root)
    else:
        suffix_name = dns.name.root

    try:
        outer_name = prefix_name.concatenate(suffix_name)
        remaining_bytes = 255 - len(outer_name)
    except dns.name.NameTooLong:
        warn(
            "Maximal length name of name execeeded by prefix and suffix. Strategy won't generate any names.",
            RuntimeWarning,
        )
        return nothing()

    remaining_labels = max_labels - len(outer_name.labels)
    if remaining_labels < 0:
        warn(
            "Maximal number of labels execeeded by prefix and suffix. Strategy won't generate any names.",
            RuntimeWarning,
        )
        return nothing()

    if not remaining_bytes or not remaining_labels:
        warn(
            f"Strategy will return only one name ({outer_name}) as it exactly matches byte or label length limit.",
            RuntimeWarning,
        )
        return just(outer_name)

    remaining_labels = min(remaining_labels, remaining_bytes // 2)

    def concatenate(labels: List[bytes]) -> dns.name.Name:
        return dns.name.Name(prefix_name.labels + tuple(labels) + suffix_name.labels)

    dns_label_list = lists(dns_labels, min_size=min_labels, max_size=max_labels).filter(
        lambda labels: sum(len(l) + 1 for l in labels) <= remaining_bytes
    )

    return builds(concatenate, dns_label_list)


dns_rdataclasses = integers(0, dns.rdataclass.RdataClass._maximum())  # type: ignore
dns_rdataclasses_without_meta = dns_rdataclasses.filter(dns.rdataclass.is_metaclass)  # type: ignore
dns_rdatatypes = integers(0, dns.rdatatype.RdataType._maximum())  # type: ignore
# This is made in this cucumbersome way to avoid filters and HealthCheck warnings complaining about the filter being too
dns_rdatatypes_without_meta = integers(0, dns.rdatatype.OPT - 1) | integers(dns.rdatatype.OPT + 1, 127) | integers(256, dns.rdatatype.RdataType._maximum())  # type: ignore

dns_queries = builds(dns.message.make_query, qname=dns_names(), rdtype=dns_rdataclasses, rdclass=dns_rdataclasses)
