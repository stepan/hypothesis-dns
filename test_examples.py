import dns.message
import dns.name
import dns.rdataclass
from hypothesis import given

from strategies import dns_names, dns_rdataclasses, dns_rdatatypes, dns_queries


@given(dns_names())
def test_dns_names_from_text_to_text(name: dns.name.Name) -> None:
    assert name == dns.name.from_text(name.to_text())


@given(dns_rdataclasses)
def test_dns_rdataclass_from_text_to_text(rdataclass: int) -> None:
    assert rdataclass == dns.rdataclass.from_text(dns.rdataclass.to_text(rdataclass))  # type: ignore


@given(dns_rdatatypes)
def test_dns_rdatatypes_from_text_to_text(rdatatype: int) -> None:
    assert rdatatype == dns.rdatatype.from_text(dns.rdatatype.to_text(rdatatype))  # type: ignore


@given(dns_queries)
def test_dns_queries_from_text_to_text(query: dns.message.Message) -> None:
    assert query == dns.message.from_text(query.to_text())
