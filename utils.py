import dns.rcode
import dns.message


def assert_noerror(message: dns.message.Message) -> None:
    assert message.rcode() == dns.rcode.NOERROR, str(message)


def assert_single_question(message: dns.message.Message) -> None:
    assert len(message.question) == 1, str(message)


def assert_empty_answer(message: dns.message.Message) -> None:
    assert not message.answer, str(message)


def assert_is_response_to(response: dns.message.Message, query: dns.message.Message) -> None:
    assert query.is_response(response), str(response)
